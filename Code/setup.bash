aws eks --region us-west-2 update-kubeconfig --name final-project-cluster
kubectl create ns jenkins
kubectl config set-context --current --namespace=jenkins
helm repo add jenkins https://charts.jenkins.io
helm repo update
helm install jenkins jenkins/jenkins
helm upgrade --install -f values.yaml jenkins jenkins/jenkins

#Finds credentials for jenkins:
kubectl exec --stdin --tty -c jenkins jenkins-0 -- bash -c "cat /run/secrets/additional/chart-admin-username && echo && cat /run/secrets/additional/chart-admin-password && echo"
