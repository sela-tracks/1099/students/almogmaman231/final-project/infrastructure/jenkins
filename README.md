# Steps for setting up Jenkins on EKS

## Description
This document outlines the steps to set up Jenkins on an Amazon Elastic Kubernetes Service (EKS) cluster.

## Prerequisites
- An EKS cluster set up and configured
- `kubectl` command-line tool installed and configured to communicate with the EKS cluster
- Helm package manager installed (version 3 or later)

## Steps


## Automation:

for setup

```
setup.bash
```

for cleaning

```
clean.bash
```


## Manual:

1. Set kubectl to work with me EKS
    ```
    aws eks --region us-west-2 update-kubeconfig --name final-project-cluster
    ```

2. Open a New Namespace for Jenkins in EKS:
    ```
    kubectl create ns jenkins
    ```

3. Modify the Working Namespace to Jenkins:
    ```
    kubectl config set-context --current --namespace=jenkins
    ```

4. Install Helm:
    Follow the instructions here: https://helm.sh/docs/intro/install/

5. Install Jenkins on EKS via Helm:
    
    5.1 Add the Jenkins Helm Chart Repository:
        ```
        helm repo add jenkins https://charts.jenkins.io
        helm repo update
        ```

    5.2 Install Jenkins via Helm:
        ```
        helm upgrade --install jenkins jenkins/jenkins
        ```
        
    **Jenkins is running**
    ![POD](Images/pod-running.PNG)

6. Find the Username and Password for Jenkins:
    
    6.1 Enter the Jenkins Pod:
        ```
        kubectl exec --stdin --tty -c jenkins jenkins-0 -- bash
        ```

    6.2 Find the Username:
        ```
        cat /run/secrets/additional/chart-admin-username && echo
        ```

    6.3 Find the Password:
        ```
        cat /run/secrets/additional/chart-admin-password && echo
        ```

    6.4 Exit the Jenkins Container by typing `exit` in the terminal.

7. Make Jenkins Accessible Publicly:
    
    7.1 Create a values.yaml file with the following content:
        ```yaml
        controller:
            serviceType: LoadBalancer
        ```

    7.2 Update Jenkins with the values.yaml:
        ```
        helm upgrade --install -f values.yaml jenkins jenkins/jenkins
        ```

    7.3 Confirm that the Jenkins service type has changed to LoadBalancer and access Jenkins publicly.
        
    **Public Access**
    ![PUBLIC](Images/jenkins-public-access.PNG)

8. Login to Jenkins with the user and password:

    **Logged in to Jenkins**
    ![LOGGED-IN](Images/jenkins-logged-in.PNG)

9. Add the Following Plugins to Jenkins by Inserting Them into the values.yaml File and Running the Update Command:
    ```
    helm upgrade --install -f values.yaml jenkins jenkins/jenkins
    ```

    Plugins List:
    - kubernetes
    - workflow-aggregator
    - git
    - configuration-as-code
    - gitlab-plugin
    - blueocean
    - workflow-multibranch
    - login-theme
    - prometheus
    - github-oauth